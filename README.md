# Hey, I'm Payton 👋

I'm a senior software engineer at GitLab. I specifically work on the frontend as part of the pipeline execution team (part of Verify). I've been on the team since August 2019 and really enjoy what I do.

I spend most of my days writing code for pipelines, jobs and merge trains. If I'm not building features or fixing bugs, I'm most likely reviewing code or helping out others in our companies Slack channel.

## How I like to work

- Async is my jam, I much prefer to communicate via issues, merge requests or Slack (I struggle with anxiety so I don't do the best in meetings sometimes, it's hit or miss with me).
- I like to work fast and get a lot of issues knocked out during each milestone.
- I enjoy collaborating and learning, I'm always up for a better way to do things (I learn a lot from code reviews).
- Sometimes I don't do well with change, I try to push myself to get out of my comfort zone but within reason.
- I'm always around to help solve problems, one of my favorite parts about software engineering is solving complex issues.

## How I review code

#### Capacity 

I don't typically take on more than 2-3 code reviews at a time. I've found if I have more than that, I can't give the MR author my best review.

#### Timebox

I usually dedicate around 15-20 minutes per code review. During that time the MR has my undivided attention. 

#### Process

I start off with a high-level overview of the code. I look for potential maintainability issues with the code etc. (code smell, duplicate code, components getting too big). After the high-level overview I move on to ensure the proper test coverage was introduced for the new code. I don't test MRs locally unless specifically requested, I trust that the MR author has done their job and tested all use-cases for their code. I review/maintain code to ensure we have a healthy codebase and that others can easily contribute to the code.

## Hobbies

- I love video games and watching anime (I'm a big nerd). If you ever see me in a meeting I have a huge collection of funko pops from some of my favorite shows.
- I enjoy lifting weights and working out. I've been working out for a long time, it's great for my mental/physical health.
- I love spending time with my wife and daughter. Family is really important to me!
- I love trucks and cars. I drive a truck and one day want a classic muscle car to take to car shows.